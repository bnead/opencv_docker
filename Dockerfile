FROM ubuntu:20.04

RUN apt-get -y update && \
    apt-get -y dist-upgrade && \
    apt-get -y autoremove && \
    apt-get -y autoclean && \
    DEBIAN_FRONTEND=noninteractive \
    apt-get -y install \
    wget \
    unzip \
    build-essential \
    cmake \
    git \
    pkg-config \
    libatlas-base-dev \
    libavcodec-dev \
    libavformat-dev \
    libgtk2.0-dev \
    libswscale-dev \
    libdc1394-22-dev \
    libgflags-dev \
    libgoogle-glog-dev \
    libjpeg-dev \
    libmetis-dev \
    libpng-dev \
    libtbb2 \
    libtbb-dev \
    libtiff-dev \
    libv4l-dev \
    libvtk6-dev

# Install the latest ceres
RUN git clone https://github.com/ceres-solver/ceres-solver.git && \
    mkdir ceres-solver/build && \
    cd ceres-solver/build && \
    cmake -DCMAKE_BUILD_TYPE=RELEASE -DBUILD_EXAMPLES=OFF -DBUILD_TESTING=OFF .. && \
    make install -j4 && \
    cd / && \
    rm -rf /ceres-solver

RUN git clone https://github.com/opencv/opencv.git && \
    git clone https://github.com/opencv/opencv_contrib && \
    mkdir opencv/build && \
    cd opencv/build && \
    cmake -D CMAKE_BUILD_TYPE=RELEASE -D INSTALL_C_EXAMPLES=OFF -D INSTALL_PYTHON_EXAMPLES=OFF -D OPENCV_EXTRA_MODULES_PATH=/opencv_contrib/modules -D BUILD_EXAMPLES=OFF -D BUILD_DOCS=OFF -D BUILD_TESTS=OFF -D BUILD_PERF_TESTS=OFF -D WITH_TBB=ON -D WITH_OPENMP=ON -D WITH_IPP=ON -D WITH_CSTRIPES=ON -D WITH_OPENCL=ON -D WITH_V4L=ON -D WITH_VTK=ON .. && \
    make install -j8 && \ 
    cd / && \
    rm -rf /opencv && \ 
    rm -rf /opencv_contrib

RUN git clone https://github.com/rmsalinas/DBow3.git && \
    mkdir DBow3/build && \
    cd DBow3/build && \
    cmake .. && \
    make install -j8 && \
    cd / && \
    rm -rf /DBow3
